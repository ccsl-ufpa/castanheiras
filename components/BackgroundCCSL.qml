import QtQuick 2.15
import QtQuick.Controls.Material 2.15

import '../js/config.js' as CF

Rectangle {
    color: CF.backgroundColor

    Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: CF.backgroundImage
        opacity: 0.6
    }
}
