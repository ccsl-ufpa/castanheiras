import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.12
import QtQuick.LocalStorage 2.12

import '../components'
import "../js/database.js" as DB

PageCCSL {
    property real latitude
    property real longitude

    PanelCCSL {
        headerText: qsTr('First section')
        width: parent.width
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            Layout.alignment: Qt.AlignCenter
            text: qsTr("Adicione Castanheiras")
            font.family: "Segoe UI"
            font.bold: true
            font.pointSize: 16
            color: "grey"
        }

        //Espaço para inserção dos dados solicitados
        RowLayout {
            Layout.alignment: Qt.AlignCenter

            TextField {
                id: averageProductionInput
                placeholderText: 'Produção média'
                placeholderTextColor: 'grey'
            }

            TextField {
                id: thicknessInput
                placeholderText: 'Grossura'
                placeholderTextColor: 'grey'
            }
        }

        //Botão de salvamento de dados e chada de banco de dados
        Button {
            id: saveButton
            Layout.alignment: Qt.AlignCenter
            text: 'Save'
            onClicked: {
                DB.insertData(averageProductionInput.text, thicknessInput.text, latitude, longitude)
                averageProductionInput.clear()
                thicknessInput.clear()
                stack.push(Mapa)
            }
        }
    }
}
