//Função que cria o banco de dados
function createDatabase(){

    try {
        var db = LocalStorage.openDatabaseSync("castanheiras", "1.0", "castanheiras database", 1000000)

        db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS castanheiras (averageProduction text, thickness text, latitude real, longitude real)')
        })
    } catch (err) {
        console.log("Error creating table in database: " + err)
    }
    return db
}

//Função que "captura" o banco de dados
function getDatabase(){

    try {
        var db = LocalStorage.openDatabaseSync("castanheiras", "1.0", "castanheiras database", 1000000)

    } catch (err) {
        console.log("Error opening database: " + err)
    }
    return db
}

//Função que adiciona dados no banco de dados
function insertData(averageProduction, thickness, latitude, longitude){

    var db = getDatabase()

    db.transaction(function (tx) {
        tx.executeSql('INSERT INTO castanheiras VALUES(?, ?, ?, ?)',[averageProduction, thickness, latitude, longitude])
    })

}

//Função que lista os elementos do banco de dados
function listPeople(){

    var db = getDatabase()

    modeListPeople.clear()

    db.transaction(function (tx) {
        var results = tx.executeSql('SELECT averageProduction, thickness FROM castanheiras')

        for (var i = 0; i < results.rows.length; i++) {
            modeListPeople.append({
                                      averageProduction: results.rows.item(i).averageProduction,
                                      thickness: results.rows.item(i).thickness,
                                  })
        }
    })
}

function showMarker(){

    var db = getDatabase()

    locationModel.clear()

    db.transaction(function (tx) {
        var results = tx.executeSql('SELECT latitude, longitude FROM castanheiras')

        for (var i = 0; i < results.rows.length; i++) {
            locationModel.append({
                                      latitude: results.rows.item(i).latitude,
                                      longitude: results.rows.item(i).longitude,
                                  })
        }
    })
}

//Função que deleta todos os elementos do banco de dados
function deleteAllPeople(){

    var db = getDatabase()

    db.transaction(function(tx){
        tx.executeSql('DELETE FROM castanheiras')

    })
}
