// Title and texts
var title = qsTr('sqmleton')
var trademark = qsTr('s<b>qml</b>eton')

// Layout
var heigth = 640
var width = 480

// Images and colors
var logoImage = 'qrc:/assets/icons/skull.svg'

var backgroundImage = 'qrc:/assets/images/background.png'
var backgroundColor = '#000'

var titlePageColor = '#FFF'
