import QtQuick 2.15

ListModel {
    id: modelPagesList

    ListElement {
        nameId: 'home'
        active: true
        title: qsTr('Inicio')
        source: 'qrc:/pages/Home.qml'
        sourceIcon: 'qrc:/assets/icons/home.svg'
    }
    ListElement {
        nameId: 'cadastro'
        active: true
        title: qsTr('Cadastro')
        source: 'qrc:/pages/Cadastro.qml'
        sourceIcon: 'qrc:/assets/icons/skull.svg'
    }
    ListElement {
        nameId: 'mapa'
        active: true
        title: qsTr('Mapa')
        source: 'qrc:/pages/Mapa.qml'
        sourceIcon: 'qrc:/assets/icons/skull.svg'
    }
    ListElement {
        nameId: 'guia'
        active: true
        title: qsTr('Guia de Manejo')
        source: 'qrc:/pages/GuiaDeManejo.qml'
        sourceIcon: 'qrc:/assets/icons/skull.svg'
    }
    ListElement {
        nameId: 'tutorial'
        active: true
        title: qsTr('Tutorial do App')
        source: 'qrc:/pages/TutorialDoApp.qml'
        sourceIcon: 'qrc:/assets/icons/skull.svg'
    }
    ListElement {
        nameId: 'about'
        active: true
        title: qsTr('Sobre')
        source: 'qrc:/pages/About.qml'
        sourceIcon: 'qrc:/assets/icons/about.svg'
    }
}
